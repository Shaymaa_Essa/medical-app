package medicalapp.its2015.com.medicalapp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by MOSTAFA on 07/04/2016.
 */
public class ClinicDataAdapter extends RecyclerView.Adapter<ClinicDataAdapter.ClinicDataHolder> {

    List<ClinicListData> ListClinicData;


    class ClinicDataHolder extends RecyclerView.ViewHolder{
        TextView textViewCity;
        TextView textViewArea;
        TextView textViewStreat;
        TextView textViewBuilding;
        TextView textViewFlat_number;
        TextView textViewWork_from;
        TextView textViewWork_to;
        TextView textViewCost;
        public ClinicDataHolder(View itemView) {
            super(itemView);
            textViewCity=(TextView)itemView.findViewById(R.id.textView_city);
            textViewArea=(TextView)itemView.findViewById(R.id.textView_area);
            textViewStreat=(TextView)itemView.findViewById(R.id.textView_streat);
            textViewBuilding=(TextView)itemView.findViewById(R.id.textView_building);
            textViewFlat_number=(TextView)itemView.findViewById(R.id.textView_flat_num);
            textViewWork_from=(TextView)itemView.findViewById(R.id.textView_workfrom);
            textViewWork_to=(TextView)itemView.findViewById(R.id.textView_workto);
            textViewCost=(TextView)itemView.findViewById(R.id.textView_cost);

        }
    }

    @Override
    public ClinicDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View row= LayoutInflater.from(parent.getContext()).inflate(R.layout.clinic_info_listview,parent,false);
        ClinicDataHolder holder=new ClinicDataHolder(row);
        return holder;
    }

    public ClinicDataAdapter(List<ClinicListData> ListClinicData){
        this.ListClinicData=ListClinicData;
    }


    @Override
    public void onBindViewHolder(ClinicDataHolder holder, int position) {
        ClinicListData data=ListClinicData.get(position);
        holder.textViewCity.setText(data.getCity());
        holder.textViewArea.setText(data.getArea());
        holder.textViewBuilding.setText(data.getBuilding());
        holder.textViewFlat_number.setText(data.getFlat_number());
        holder.textViewWork_from.setText(data.getWork_from());
        holder.textViewWork_to.setText(data.getWork_to());
        holder.textViewCost.setText(data.getCost());
    }





    @Override
    public int getItemCount() {
        return ListClinicData.size();
    }












}
