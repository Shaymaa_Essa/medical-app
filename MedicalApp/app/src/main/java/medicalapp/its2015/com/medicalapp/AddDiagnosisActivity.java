package medicalapp.its2015.com.medicalapp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class AddDiagnosisActivity extends ActionBarActivity {

    TextView patientName;
    TextView doctorName;
    EditText diagnosis;
    EditText nextVisit;
    EditText notes;
    Button save;
    Button addMedicine;


    String url;
    String diagnosisId;

    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;

    public static final String PATIENT_NAME = "patientName";
    public static final String PATIENT_ID = "patientId";
    String patientNameValue;
    int patientIdValue;
    String doctorId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_diagnosis);
        initialize();

        //to set date on nextvisit field
        myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        nextVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddDiagnosisActivity.this, date,
                        myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        doctorName.setText(((MyAppConstants) this.getApplication()).getUserFirstName() + " " + ((MyAppConstants) this.getApplication()).getUserLastName());
        doctorId = ((MyAppConstants) this.getApplication()).getDoctorId();


        Intent i = getIntent();
        patientNameValue = i.getStringExtra(PATIENT_NAME);
        patientName.setText(patientNameValue);
        patientIdValue = i.getIntExtra(PATIENT_ID,0);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()){
                    webservice();
                }
            }
        });

    }

    private void webservice() {
        url = "http://medicalapp.site88.net/phpfiles/doctor_patients_newdiagnosis4.php";
        StringRequest postrequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.contains("diagnosis record inserted")){
                    try{
                        JSONObject diagnosisInfo = new JSONObject(response);
                        diagnosisId = diagnosisInfo.getString("diagnosis_id");

                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                    Toast.makeText(AddDiagnosisActivity.this, "diagnosis record inserted", Toast.LENGTH_LONG).show();
                    addMedicine.setEnabled(true);
                    save.setEnabled(false);
                    addMedicine.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent medicineDetail = new Intent(AddDiagnosisActivity.this,MedicineDetailActivity.class);
                            medicineDetail.putExtra(MedicineDetailActivity.DIAGNOSIS_ID,diagnosisId);
                            startActivity(medicineDetail);
                        }
                    });



                }else{
                    Toast.makeText(AddDiagnosisActivity.this,response,Toast.LENGTH_LONG).show();
                }


            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(AddDiagnosisActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap params = new HashMap();
                params.put("user_id", String.valueOf(patientIdValue));
                params.put("doctor_id", doctorId);
                params.put("diagnosis",diagnosis.getText().toString());
                params.put("next_visit",nextVisit.getText().toString());
                params.put("notes",notes.getText().toString());

                return params;

            }

        };
        Volley.newRequestQueue(this).add(postrequest);
    }

    private void initialize() {
        patientName = (TextView)findViewById(R.id.txt_adddiagnosis_patientname);
        doctorName = (TextView)findViewById(R.id.txt_adddiagnosis_doctorname);
        diagnosis = (EditText)findViewById(R.id.etxt_adddiagnosis_diagnosis);
        nextVisit = (EditText)findViewById(R.id.etxt_adddiagnosis_nextvisit);
        notes = (EditText)findViewById(R.id.etxt_adddiagnosis_notes);
        save = (Button)findViewById(R.id.btn_adddiagnosis_save);
        addMedicine = (Button)findViewById(R.id.btn_adddiagnosis_addmedicine);
    }


    public boolean validation(){
        String diagnosisValue = diagnosis.getText().toString();
        String nextVisitValue = nextVisit.getText().toString();

        if(TextUtils.isEmpty(diagnosisValue)) {
            diagnosis.setError("Please Enter The diagnosis");
            return false;
        }

        //to validate NextVisit
        if(!TextUtils.isEmpty(nextVisitValue)) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date myDate;
            try {
                myDate = df.parse(nextVisit.getText().toString());
                Calendar cal = Calendar.getInstance();
                Calendar cal2 = Calendar.getInstance();
                cal.setTime(myDate);
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                if ((year < cal2.get(Calendar.YEAR))||(month < cal2.get(Calendar.MONTH)) || (day <cal2.get(Calendar.DAY_OF_MONTH))) {
                    nextVisit.setError("Please Enter a next Valid date");
                    return false;
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    //to set date on nextvisit field
    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        nextVisit.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_diagnosis, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
