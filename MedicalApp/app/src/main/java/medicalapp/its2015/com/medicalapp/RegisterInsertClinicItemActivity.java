package medicalapp.its2015.com.medicalapp;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


public class RegisterInsertClinicItemActivity extends ActionBarActivity{
    EditText lat;
    EditText lng;
    EditText city;
    EditText area;
    EditText street;
    EditText building;
    EditText flatNumber;
    TimePicker workFrom;
    TimePicker workTo;
    EditText cost;
    Button save;

    String url;

    static final String DOCTOR_ID = "doctor_id";
    String doctor_id;



    int hour;
    int min;

    String workFromFormatted;
    String workToFormatted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_insert_clinic_item);

        Intent i = getIntent();
        doctor_id = i.getStringExtra(DOCTOR_ID);

        initialize();
        save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (validate()){
                        int workFromMiliSec = (workFrom.getCurrentMinute() * 60 + workFrom.getCurrentHour() * 60 * 60) * 1000;
                        int workToMiliSec = (workTo.getCurrentMinute() * 60 + workTo.getCurrentHour() * 60 * 60) * 1000;

                        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
                         workFromFormatted = format.format(workFromMiliSec);
                         workToFormatted = format.format(workToMiliSec);


                        webservice();

                    }

                }
            });


    }

    private void webservice() {
        url = "http://medicalapp.site88.net/phpfiles/clinicaddressinsert.php";
        StringRequest postrequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.contains("Clinic_address created")){
                    Toast.makeText(RegisterInsertClinicItemActivity.this, "Info Saved", Toast.LENGTH_LONG).show();
                    //getTheResponce();
                }

                else {
                    Toast.makeText(RegisterInsertClinicItemActivity.this, response, Toast.LENGTH_LONG).show();
                }

                finish();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(RegisterInsertClinicItemActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                HashMap map  = new HashMap();
                map.put("doctor_id",doctor_id);

                if(lng.getText().toString() !=null)
                map.put("longitude",lng.getText().toString());

                if(lat.getText().toString() !=null)
                map.put("latitude",lat.getText().toString());


                map.put("city",city.getText().toString());

                if(area.getText().toString() !=null)
                map.put("area", area.getText().toString());

                if(street.getText().toString() !=null)
                map.put("streat",street.getText().toString());

                if(building.getText().toString() !=null)
                map.put("building",building.getText().toString());

                if(flatNumber.getText().toString() !=null)
                map.put("flat_number",flatNumber.getText().toString());

                map.put("work_from",workFromFormatted);
                map.put("work_to",workToFormatted);

                if(cost.getText().toString() !=null)
                map.put("cost",cost.getText().toString());




                return map;
            }
        };

        Volley.newRequestQueue(this).add(postrequest);
    }


    private boolean validate() {
        String cityValue = city.getText().toString();

        if(TextUtils.isEmpty(cityValue) ) {
            city.setError("Please Enter City");
            return false;
        }


        return  true;
    }

    private void initialize() {
        lat = (EditText)findViewById(R.id.etxt_registerinsertclinicitem_latitude);
        lng = (EditText)findViewById(R.id.etxt_registerinsertclinicitem_longitude);
        city = (EditText)findViewById(R.id.etxt_registerinsertclinicitem_city);
        area = (EditText)findViewById(R.id.etxt_registerinsertclinicitem_area);
        street = (EditText)findViewById(R.id.etxt_registerinsertclinicitem_street);
        building = (EditText)findViewById(R.id.etxt_registerinsertclinicitem_building);
        flatNumber = (EditText)findViewById(R.id.etxt_registerinsertclinicitem_flatnumber);
        workFrom = (TimePicker)findViewById(R.id.timepicker_registerinsertclinicitem_workfrom);
        workTo = (TimePicker)findViewById(R.id.timepicker_registerinsertclinicitem_workto);
        cost = (EditText)findViewById(R.id.etxt_registerinsertclinicitem_cost);
        save = (Button)findViewById(R.id.btn_registerinsertclinicitem_save);

        //initialize work from and work to time picker
        final Calendar c = Calendar.getInstance();
        hour= c.get(Calendar.HOUR_OF_DAY);
        min = c.get(Calendar.MINUTE);
        workFrom.setCurrentHour(0);
        workFrom.setCurrentMinute(0);
        workTo.setCurrentHour(0);
        workTo.setCurrentMinute(0);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register_insert_clinic_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
