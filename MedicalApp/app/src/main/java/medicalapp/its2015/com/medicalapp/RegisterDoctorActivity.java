package medicalapp.its2015.com.medicalapp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class RegisterDoctorActivity extends ActionBarActivity {
    public static final String MOBILE_NUMBER = "mobilenumber";


    EditText specialist;
    EditText qualifications;
    Button save;
    Button addClinic;
    Button finishRegister;

    String url;
    String mobileNumber;
    String doctorId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_doctor);

        specialist = (EditText)findViewById(R.id.etxt_registerdoctor_specialist);
        qualifications = (EditText)findViewById(R.id.etxt_registerdoctor_qualifications);
        save = (Button)findViewById(R.id.btn_registerdoctor_save);
        addClinic = (Button)findViewById(R.id.btn_registerdoctor_addclinicaddress);
        finishRegister = (Button)findViewById(R.id.btn_registerdoctor_finishregistration);

        Intent intent = getIntent();
        mobileNumber = intent.getStringExtra(MOBILE_NUMBER);


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()){
                    webservice();
                }



            }
        });


    }

    private void webservice(){

        url = "http://medicalapp.site88.net/phpfiles/register_doctor_info.php";
        StringRequest postrequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("created")) {
                    Toast.makeText(RegisterDoctorActivity.this,"Info saved",Toast.LENGTH_LONG).show();
                    try {
                        JSONObject userInfo = new JSONObject(response);
                        //JSONArray patientArray = userInfo.getJSONArray("userinfo");

                        //JSONObject currentObject = userInfo.getString("doctor_id");
                        doctorId = userInfo.getString("doctor_id");


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    save.setEnabled(false);
                    addClinic.setEnabled(true);
                    finishRegister.setEnabled(true);

                    addClinic.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(RegisterDoctorActivity.this, RegisterInsertClinicItemActivity.class);
                            i.putExtra(RegisterInsertClinicItemActivity.DOCTOR_ID, doctorId);

                            startActivity(i);
                        }
                    });

                    finishRegister.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent loginScreen = new Intent(RegisterDoctorActivity.this,LoginActivity.class);
                            startActivity(loginScreen);
                        }
                    });
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(RegisterDoctorActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                HashMap map  = new HashMap();
                map.put("mobile_number",mobileNumber);
                map.put("specialization",specialist.getText().toString());
                map.put("Qualifications",qualifications.getText().toString());

                return map;
            }
        };

        Volley.newRequestQueue(this).add(postrequest);

    }

    public boolean validation() {
        String specialistValue = specialist.getText().toString();


        if (TextUtils.isEmpty(specialistValue)) {
            specialist.setError("Please Enter Your Specialist");
            return false;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register_doctor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    public void onBackPressed() {
//        Intent mainActivity = new Intent(Intent.ACTION_MAIN);
//        mainActivity.addCategory(Intent.CATEGORY_HOME);
//        mainActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(mainActivity);
//        finish();
//    }
}
