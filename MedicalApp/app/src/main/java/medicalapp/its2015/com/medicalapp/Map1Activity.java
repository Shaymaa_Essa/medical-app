package medicalapp.its2015.com.medicalapp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class Map1Activity extends ActionBarActivity {

    Button hospital;
    Button pharmacy;
    Button doctor;

    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map1);

        hospital = (Button) findViewById(R.id.btn_map1activity_hospital);
        pharmacy = (Button) findViewById(R.id.btn_map1activity_pharmacy);
        doctor = (Button) findViewById(R.id.btn_map1activity_Doctor);

        hospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               i = new Intent(Map1Activity.this,Map2Activity.class);
               i.putExtra(Map2Activity.SEARCH_TYPE,"hospital");
                startActivity(i);
            }
        });

        pharmacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i = new Intent(Map1Activity.this,Map2Activity.class);
                i.putExtra(Map2Activity.SEARCH_TYPE,"pharmacy");
                startActivity(i);
            }
        });

        doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i = new Intent(Map1Activity.this,Map2Activity.class);
                i.putExtra(Map2Activity.SEARCH_TYPE,"doctor");
                startActivity(i);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
