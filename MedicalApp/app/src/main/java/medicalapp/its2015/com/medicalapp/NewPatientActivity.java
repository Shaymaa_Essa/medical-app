package medicalapp.its2015.com.medicalapp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class NewPatientActivity extends ActionBarActivity {

    EditText firstName;
    EditText lastName;
    EditText mobileNumber;
    EditText email;
    EditText birthdate;
    RadioGroup gender;
    RadioButton selectedGenderButton;
    Button save;
    Button addDiagnosis;

    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;

    boolean userCreatedFlag= false;
    String url;
    int genderValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_patient);

        initialize();

        //to set date on birthdate field
        myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        birthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(NewPatientActivity.this, date,
                        myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()){
                    webservice();
                }
            }
        });

        addDiagnosis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(NewPatientActivity.this,AddDiagnosisActivity.class);
                i.putExtra(AddDiagnosisActivity.PATIENT_NAME,firstName.getText().toString()+" "+lastName.getText()
                .toString());
                startActivity(i);
            }
        });
    }

    private void webservice(){

        //to get the value of gender (Male = 0 and female =1) and type(doctor = 0,patient=1)
        int selectedGender = gender.getCheckedRadioButtonId();
        selectedGenderButton = (RadioButton)findViewById(selectedGender);
        if (selectedGender == R.id.radio_register_gendermale) //male radio button
            genderValue = 0;
        else  if (selectedGender == R.id.radio_register_genderfemale) //female radio button
            genderValue = 1;



        url = "http://medicalapp.site88.net/phpfiles/register.php";
        StringRequest postrequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.contains("created")){
                    Toast.makeText(NewPatientActivity.this, "Account Created", Toast.LENGTH_LONG).show();
                    boolean testButton = getTheResponce();
                    if (userCreatedFlag){  //type = doctor so open a new screen with qualification and specification info
                        addDiagnosis.setEnabled(true);
                        save.setEnabled(false);
                    }
                }

                else {
                    Toast.makeText(NewPatientActivity.this, response, Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(NewPatientActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {



                HashMap map  = new HashMap();
                map.put("first_name",firstName.getText().toString());
                map.put("last_name",lastName.getText().toString());
                map.put("email",email.getText().toString());
                map.put("password","no data available");
                map.put("mobile_number",mobileNumber.getText().toString());
                map.put("gender",Integer.toString(genderValue));
                map.put("type","1");
                if (birthdate.getText().toString()!="")
                    map.put("birthdate",birthdate.getText().toString());
                //upload photo to server
                //if(test.getText().toString()!="")
                //code to upload photo

                return map;
            }
        };

        Volley.newRequestQueue(this).add(postrequest);

    }

    public  boolean getTheResponce(){

        userCreatedFlag = true;
        return  true;
    }

    //to set date on birthdate field
    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        birthdate.setText(sdf.format(myCalendar.getTime()));
    }

    private void initialize() {
        firstName = (EditText)findViewById(R.id.etxt_newpatient_firstname);
        lastName = (EditText)findViewById(R.id.etxt_newpatient_lastname);
        mobileNumber = (EditText)findViewById(R.id.etxt_newpatient_mobilenumber);
        birthdate = (EditText)findViewById(R.id.etxt_newpatient_birthdate);
        gender = (RadioGroup)findViewById(R.id.rgp_newpatient_radiogender);
        save = (Button)findViewById(R.id.btn_newpatients_save);
        addDiagnosis = (Button)findViewById(R.id.btn_newpatient_adddiagnosis);
        email = (EditText)findViewById(R.id.etxt_newpatient_email);


    }

    public boolean validation() {
        String testFirstname = firstName.getText().toString();
        String testLastname = lastName.getText().toString();
        String testMobilenumber = mobileNumber.getText().toString();
        String testBirthdate = birthdate.getText().toString();
        String testEmail = email.getText().toString();

        if(TextUtils.isEmpty(testFirstname) || testFirstname.length() < 2) {
            firstName.setError("Please Enter a valid fristname");
            return false;
        }
        if(TextUtils.isEmpty(testLastname) || testLastname.length() < 3) {
            lastName.setError("Please Enter lastname");
            return false;
        }
        if(TextUtils.isEmpty(testMobilenumber) || !(testMobilenumber.length()==11)) {
            mobileNumber.setError("Please Enter a valid mobile number");
            return false;
        }

        if(TextUtils.isEmpty(testEmail) || !testEmail.contains("@") || !testEmail.contains(".")) {
            email.setError("Please Enter a valid email");
            return false;
        }

        //to validate birthdate
        if(!TextUtils.isEmpty(testBirthdate)) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date myDate;
            try {
                myDate = df.parse(birthdate.getText().toString());
                Calendar cal = Calendar.getInstance();
                Calendar cal2 = Calendar.getInstance();
                cal.setTime(myDate);
                int year = cal.get(Calendar.YEAR);
                if (year >= cal2.get(Calendar.YEAR)) {
                    birthdate.setError("Please Enter a valid birthdate");
                    return false;
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_patient, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
