package medicalapp.its2015.com.medicalapp;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MOSTAFA on 23/03/2016.
 */
public class PatientAdapter extends ArrayAdapter<Patient>  {
    List<Patient> patients = new ArrayList<Patient>();
    List<Patient> originalPatientsList ;
    Context context;
    TextView name;
    Button addDiagnosis;
    Button medicalHistory;
    //ItemFilter mFilter = new ItemFilter();


    public PatientAdapter (Context context,int txtResourceId, List<Patient> patients){
        super(context,txtResourceId,patients);
        this.patients = patients;
        this.context = context;
        this.originalPatientsList = new ArrayList<Patient>(patients);
    }

    public View getView (final int position , View convertView , ViewGroup parent){
        View v = convertView;
        if (v == null){
            LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.patientitem,parent,false);
        }

        Patient patient = patients.get(position);
        if(patient!= null){
            name = (TextView)v.findViewById(R.id.txt_patientitem_patientname);
            addDiagnosis = (Button)v.findViewById(R.id.btn_patientitem_adddiagnosis);
            medicalHistory = (Button)v.findViewById(R.id.btn_patientitem_medicalhistory);
            addDiagnosis.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    startNewActivity(position);
                }
            });

            medicalHistory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startNewActivity2(position);
                }
            });

            if(name != null)
                name.setText(patient.getPatientName());
        }

        return v;

    }

    private void startNewActivity2(int position) {
        Intent newMedicalHistory = new Intent(getContext(),PatientMedicalHistoryActivity.class);
        newMedicalHistory.putExtra(PatientMedicalHistoryActivity.USER_NAME,patients.get(position).getPatientName());
        newMedicalHistory.putExtra(PatientMedicalHistoryActivity.USER_ID,patients.get(position).getPatientId());
        getContext().startActivity(newMedicalHistory);
    }

    @Override
    public int getCount() {
        if(patients != null) return patients.size();
        else return 0;
    }

    public void startNewActivity(int position){
        Intent i = new Intent(getContext(),AddDiagnosisActivity.class);
        i.putExtra(AddDiagnosisActivity.PATIENT_NAME,patients.get(position).getPatientName().toString());
        i.putExtra(AddDiagnosisActivity.PATIENT_ID,patients.get(position).getPatientId());
        getContext().startActivity(i);
    }



    @Override
    public Patient getItem(int position) {
        if(patients!=null) return patients.get(position);
        else return  null;
    }

    @Override
    public long getItemId(int position) {
        if(patients!= null) return patients.get(position).hashCode();
        else return 0;
    }

    public List<Patient> getItemList(){
        return patients;
    }

    public void setItemList (List<Patient> patients){
        this.patients = patients;
    }

//    @Override
//    public Filter getFilter() {
//        return mFilter;
//    }
//
//    private class ItemFilter extends Filter{
//
//        @Override
//        protected FilterResults performFiltering(CharSequence charSequence) {
//            String filterString = charSequence.toString().toLowerCase(); //string which i search for
//            FilterResults results = new FilterResults(); //result which will be returned
//            results.values = originalPatientsList;
//            results.count = originalPatientsList.size();
//            return results;
//        }
//
//        @Override
//        protected void publishResults(CharSequence charSequence, FilterResults results) {
//            patients.clear();
//            patients.addAll((ArrayList<Patient>)results.values);
//            notifyDataSetChanged();
//        }
//    }
}
