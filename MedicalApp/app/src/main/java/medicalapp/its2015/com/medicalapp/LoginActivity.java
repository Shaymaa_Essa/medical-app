package medicalapp.its2015.com.medicalapp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


//login activity for doctor and patient
public class LoginActivity extends ActionBarActivity {
    EditText username_et;
    EditText password_et;
    Button login_btn;
    TextView register_txt;

    private static final int REQUEST_SIGNUP = 0;
    private static final String TAG = "LoginActivity";
    String url;
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PASSWORD = "password";
    public static final String LOGIN_SUCCESS = "success";

    String doctorId ;
    String userId;
    String userFirstName;
    String userLastName;
    String userType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        username_et = (EditText) findViewById(R.id.editText_username);
        password_et = (EditText) findViewById(R.id.editText_password);
        login_btn = (Button) findViewById(R.id.btn_login);
        register_txt = (TextView) findViewById(R.id.textView_register);
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = username_et.getText().toString();
                String Password = password_et.getText().toString();
                if (username.isEmpty() || Password.isEmpty()) {

                    Toast.makeText(getApplicationContext(), "You Must Enter Your Email And Password", Toast.LENGTH_LONG).show();
                } else {
                    checkLogin();
                    //Toast.makeText(getApplicationContext(), "Authanticated", Toast.LENGTH_SHORT).show();
                }

            }
        });


        register_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivityForResult(i, REQUEST_SIGNUP);
            }
        });


    }


    //webservice
    private void checkLogin() {

        url = "http://medicalapp.site88.net/phpfiles/login.php";
        StringRequest postrequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.contains("userinfo")){
                    try{
                        JSONObject userInfo = new JSONObject(response);
                        JSONArray patientArray = userInfo.getJSONArray("userinfo");

                        JSONObject currentObject = patientArray.getJSONObject(0);
                        userId = currentObject.getString("user_id");
                        doctorId = currentObject.getString("doctor_id");
                        userFirstName = currentObject.getString("first_name");
                        userLastName = currentObject.getString("last_name");
                        userType = currentObject.getString("type");

                        // set the variables in th global variable in class MyAppConstants
                        setGlobalVar();





                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                    Toast.makeText(LoginActivity.this,"welcome",Toast.LENGTH_LONG).show();

                    if (userType.contains( "0")){
                        Intent i  = new Intent(LoginActivity.this,HomeActivity.class);
                        startActivity(i);
                    }
                    else if (userType.contains("1")){
                        Intent i  = new Intent(LoginActivity.this,Patient_HomeActivity.class);
                        startActivity(i);
                    }

                    else{
                        Toast.makeText(LoginActivity.this,"Can't determine user type",Toast.LENGTH_LONG).show();
                    }

                }else{
                    Toast.makeText(LoginActivity.this,response,Toast.LENGTH_LONG).show();
                }


            }




        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap params = new HashMap();
                params.put(KEY_EMAIL, username_et.getText().toString());
                params.put(KEY_PASSWORD, password_et.getText().toString());

                return params;

            }

        };
        Volley.newRequestQueue(this).add(postrequest);
    }

    public void setGlobalVar(){
        ((MyAppConstants) this.getApplication()).setUserId(userId);
        ((MyAppConstants) this.getApplication()).setDoctorId(doctorId);
        ((MyAppConstants) this.getApplication()).setUserFirstName(userFirstName);
        ((MyAppConstants) this.getApplication()).setUserLastName(userLastName);
        ((MyAppConstants) this.getApplication()).setUserEmail(username_et.getText().toString());
    }
}





