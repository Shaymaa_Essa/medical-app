package medicalapp.its2015.com.medicalapp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


//doctor insert the medicine one by one and saved in database after saving diagnosis
public class MedicineDetailActivity extends ActionBarActivity {

    static final String DIAGNOSIS_ID  = "diagnosisId";
    String diagnosisId;

    EditText medicineName;
    EditText startDate;
    EditText endDate;
    EditText time;
    EditText notes;
    Button save;

    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener dateStart;
    DatePickerDialog.OnDateSetListener dateEnd;

    String url;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_detail);

        initialize();

        //-------------------------to set date on startdate and enddate fields ----------------------
        myCalendar = Calendar.getInstance();
        dateStart = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabelStart();
            }

        };
        dateEnd = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabelEnd();
            }

        };
        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(MedicineDetailActivity.this, dateStart,
                        myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });
        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(MedicineDetailActivity.this, dateEnd,
                        myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        //------------------------------------------

        Intent i = getIntent();
        diagnosisId = i.getStringExtra(DIAGNOSIS_ID);

        save.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                if (validation()){
                    webService();
                }
            }
        });



    }

    private void webService() {
        url = "http://medicalapp.site88.net/phpfiles/doctor_patients_insertmedicine5.php";
        StringRequest postrequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.contains("medicine record inserted")){
                    Toast.makeText(MedicineDetailActivity.this, "medicine record inserted", Toast.LENGTH_LONG).show();
                    finish();

                }else{
                    Toast.makeText(MedicineDetailActivity.this,response,Toast.LENGTH_LONG).show();
                }


            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MedicineDetailActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap params = new HashMap();
                params.put("diagnosis_id", diagnosisId);
                params.put("medicine_name", medicineName.getText().toString());
                params.put("start_date",startDate.getText().toString());
                params.put("end_date",endDate.getText().toString());
                params.put("time",time.getText().toString());
                params.put("notes",notes.getText().toString());

                return params;

            }

        };
        Volley.newRequestQueue(this).add(postrequest);
    }

    private void initialize() {
        medicineName = (EditText)findViewById(R.id.etxt_medicinedetail_medicinename);
        startDate = (EditText)findViewById(R.id.etxt_medicinedetail_startdate);
        endDate = (EditText)findViewById(R.id.etxt_medicinedetail_enddate);
        time = (EditText)findViewById(R.id.etxt_medicinedetail_time);
        notes = (EditText)findViewById(R.id.etxt_medicinedetail_notes);
        save  = (Button)findViewById(R.id.btn_medicinedetail_save);
    }

    //to set date on startdate field
    private void updateLabelStart() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        startDate.setText(sdf.format(myCalendar.getTime()));
    }

    //to set date on enddate field
    private void updateLabelEnd() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        endDate.setText(sdf.format(myCalendar.getTime()));
    }

    //validate inserted values
    public boolean validation(){
        String medicineNameValue = medicineName.getText().toString();
        String startDateValue = startDate.getText().toString();
        String endDateValue = endDate.getText().toString();

        if(TextUtils.isEmpty(medicineNameValue)) {
            medicineName.setError("Please Enter The Medicine Name");
            return false;
        }

        //to validate NextVisit
        if(!TextUtils.isEmpty(startDateValue)) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date myDate;
            try {
                myDate = df.parse(startDate.getText().toString());
                Calendar cal = Calendar.getInstance();
                Calendar cal2 = Calendar.getInstance();
                cal.setTime(myDate);
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                if ((year < cal2.get(Calendar.YEAR))||(month < cal2.get(Calendar.MONTH)) || (day <cal2.get(Calendar.DAY_OF_MONTH))) {
                    startDate.setError("Start Date must be next to today");
                    return false;
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(!TextUtils.isEmpty(endDateValue)) {
            if (TextUtils.isEmpty(startDateValue)) {
                startDate.setError("You must insert start date if you want to determine end date");
                return false;
            }
        }


        if(!TextUtils.isEmpty(endDateValue) && !TextUtils.isEmpty(startDateValue)) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date myStartDate;
            Date myEndDate;
            try {
                myStartDate = df.parse(startDate.getText().toString());
                myEndDate = df.parse(endDate.getText().toString());

                if (myStartDate.after(myEndDate)){
                    endDate.setError("End date must be after start date");
                    return false;
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_medicine_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
