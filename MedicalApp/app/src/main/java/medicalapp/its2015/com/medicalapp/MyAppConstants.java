package medicalapp.its2015.com.medicalapp;

import android.app.Application;

/**
 * Created by MOSTAFA on 30/03/2016.
 */
public class MyAppConstants  extends Application{
    //to keep constant variables globally so all activities can access them

    private  String doctorId;
    private String userId;
    private String userFirstName;
    private String userLastName;
    String userEmail;

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
