package medicalapp.its2015.com.medicalapp;

/**
 * Created by MOSTAFA on 07/04/2016.
 */
public class ClinicListData {

    public  String city;
    public  String area;
    public  String streat;
    public  String building;
    public  String flat_number;
    public  String work_from;
    public  String work_to;
    public  String cost;

    public ClinicListData(String city,  String area, String streat, String building
            , String flat_number, String work_from, String work_to, String cost){


        this.city=city;
        this.area=area;
        this.streat=streat;
        this.building=building;
        this.flat_number=flat_number;
        this.work_from=work_from;
        this.work_to=work_to;
        this.cost=cost;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getStreat() {
        return streat;
    }

    public void setStreat(String streat) {
        this.streat = streat;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getFlat_number() {
        return flat_number;
    }

    public void setFlat_number(String flat_number) {
        this.flat_number = flat_number;
    }

    public String getWork_from() {
        return work_from;
    }

    public void setWork_from(String work_from) {
        this.work_from = work_from;
    }

    public String getWork_to() {
        return work_to;
    }

    public void setWork_to(String work_to) {
        this.work_to = work_to;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }
}
