package medicalapp.its2015.com.medicalapp;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class HomeActivity extends ActionBarActivity {
    private String[] drawerTitles;
    private  int[] drawerIcons;
    private ListView drawerList;
    private DrawerLayout drawerLayout;
    TextView name;
    ListView todayPatients;
    RecyclerView navigationMenu;
    NavigationDrawerAdapter navigationDrawerAdapter;
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout

    ActionBarDrawerToggle mDrawerToggle;

    String nameValue;
    String doctorIdValue;
    String emailValue;

    PatientAdapter patientAdapter;
    List<Patient> originalPatientsList ;
    List<Patient> currentPatientsList = new ArrayList<Patient>(); //for search filter
    String url;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        drawerTitles = getResources().getStringArray(R.array.drawer_titles);
        drawerIcons = new int[]{R.drawable.personal_info,R.drawable.current_patients,R.drawable.searchforplace};
        todayPatients  = (ListView)findViewById(R.id.lview_home_todaypatients);

        nameValue = ((MyAppConstants) this.getApplication()).getUserFirstName()+" "+
                ((MyAppConstants) this.getApplication()).getUserLastName();
        emailValue = ((MyAppConstants) this.getApplication()).getUserEmail();
        doctorIdValue = ((MyAppConstants) this.getApplication()).getDoctorId();

        navigationMenu = (RecyclerView) findViewById(R.id.recview_home_navigationdrawer);
        navigationMenu.setHasFixedSize(true);
        navigationDrawerAdapter = new NavigationDrawerAdapter(drawerTitles,drawerIcons,nameValue,emailValue,R.drawable.doctorimg,getApplicationContext(),0);


        name = (TextView)findViewById(R.id.txt_home_welcome);
        name.setText("Welcome DR\\ "+nameValue);


        navigationMenu.setAdapter(navigationDrawerAdapter);                              // Setting the adapter to RecyclerView

        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager

        navigationMenu.setLayoutManager(mLayoutManager);                 // Setting the layout Manager


        webservice();

        patientAdapter = new PatientAdapter(this,R.layout.patientitem,currentPatientsList);
        todayPatients.setAdapter(patientAdapter);
    }

    private void webservice() {
        url = "http://medicalapp.site88.net/phpfiles/doctor_patientstoday.php";
        StringRequest postrequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.contains("patients")){
                    try{
                        JSONObject patientObject = new JSONObject(response);
                        JSONArray patientArray = patientObject.getJSONArray("patients");
                        for (int i =0 ;i<patientArray.length();i++){
                            JSONObject currentObject = patientArray.getJSONObject(i);
                            String patientId = currentObject.getString("user_id");
                            String patientName = currentObject.getString("patient_name");
                            currentPatientsList.add(new Patient(patientId,patientName));
                        }

                        originalPatientsList = new ArrayList<Patient>();
                        for (Patient patient : currentPatientsList){
                            originalPatientsList.add(patient);
                        }
                        patientAdapter.setItemList(currentPatientsList);
                        patientAdapter.notifyDataSetChanged();


                    }catch (JSONException e){
                        e.printStackTrace();
                    }



                }

                else {
                    Toast.makeText(HomeActivity.this, "No Patients for today"+response, Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(HomeActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap params = new HashMap();
                params.put("doctor_id", doctorIdValue);


                return params;

            }
        };

        Volley.newRequestQueue(this).add(postrequest);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
