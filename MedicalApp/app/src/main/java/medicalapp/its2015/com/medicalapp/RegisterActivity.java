package medicalapp.its2015.com.medicalapp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.*;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import android.text.TextUtils;
import android.text.TextWatcher;


public class RegisterActivity extends ActionBarActivity {


    EditText firstname;
    EditText lastname;
    EditText email;
    EditText password;
    EditText confirmpassword;
    EditText birthdate;
    EditText mobilenumber;
    Button createaccount;
    Button uploadphoto;
    TextView test;
    RadioGroup gender;
    RadioButton selectedGenderButton;
    RadioGroup type;
    RadioButton selectedTypeButton;
    RadioButton male;
    RadioButton female;
    RadioButton doctor;
    RadioButton patient;


    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;

    private static int RESULT_LOAD_IMAGE = 1;

    String url;
    int genderValue;
    int typeValue=5;
    boolean userCreatedFlag = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        birthdate = (EditText)findViewById(R.id.etxt_register_birthdate);
        uploadphoto = (Button) findViewById(R.id.btn_register_photo);
        test = (TextView) findViewById(R.id.txt_register_photo);
        createaccount = (Button) findViewById(R.id.btn_register_createaccount);
        firstname = (EditText)findViewById(R.id.etxt_register_firstname);
        lastname = (EditText)findViewById(R.id.etxt_register_lastname);
        email = (EditText)findViewById(R.id.etxt_register_email);
        password = (EditText) findViewById(R.id.etxt_register_password);
        birthdate = (EditText)findViewById(R.id.etxt_register_birthdate);
        mobilenumber = (EditText)findViewById(R.id.etxt_register_mobilenumber);
        gender = (RadioGroup)findViewById(R.id.rgp_radiogender);
        type = (RadioGroup)findViewById(R.id.rgp_radiotype);
        confirmpassword = (EditText)findViewById(R.id.etxt_register_confirmpassword);
        male = (RadioButton)findViewById(R.id.radio_register_gendermale);
        female = (RadioButton)findViewById(R.id.radio_register_genderfemale);
        doctor = (RadioButton)findViewById(R.id.radio_register_typedoctor);


        //to set date on birthdate field
        myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        birthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(RegisterActivity.this,date,
                        myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        //get photo from galary
        uploadphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        createaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()){
                    webservice();
                if (typeValue == 0 && userCreatedFlag){  //type = doctor so open a new screen with qualification and specification info
                   Intent intent = new Intent(RegisterActivity.this,RegisterDoctorActivity.class);
                  intent.putExtra(RegisterDoctorActivity.MOBILE_NUMBER,mobilenumber.getText().toString());
                   startActivity(intent);
                }
                    else if (typeValue == 1 && userCreatedFlag){
                    Intent i = new Intent(RegisterActivity.this,LoginActivity.class);
                    startActivity(i);
                }
                }
            }
        });


    }


    private void webservice(){

        //to get the value of gender (Male = 0 and female =1) and type(doctor = 0,patient=1)
        int selectedGender = gender.getCheckedRadioButtonId();
        int selectedType = type.getCheckedRadioButtonId();
        selectedGenderButton = (RadioButton)findViewById(selectedGender);
        selectedTypeButton = (RadioButton)findViewById(selectedType);
        if (selectedGender == R.id.radio_register_gendermale) //male radio button
            genderValue = 0;
        else  if (selectedGender == R.id.radio_register_genderfemale) //female radio button
            genderValue = 1;
        if (selectedType == R.id.radio_register_typedoctor)//doctor type
            typeValue = 0;
        else  if (selectedType == R.id.radio_register_typepatient) //patient type
            typeValue = 1;


        url = "http://medicalapp.site88.net/phpfiles/register.php";
        StringRequest postrequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.contains("created")){
                    Toast.makeText(RegisterActivity.this, "Account Created", Toast.LENGTH_LONG).show();
                   boolean testButton = getTheResponce();
                    if (typeValue == 0 && userCreatedFlag){  //type = doctor so open a new screen with qualification and specification info
                        Intent intent = new Intent(RegisterActivity.this,RegisterDoctorActivity.class);
                        intent.putExtra(RegisterDoctorActivity.MOBILE_NUMBER,mobilenumber.getText().toString());
                        startActivity(intent);
                    }
                    else if(typeValue ==1 && userCreatedFlag){ //type = patient so back to login screen
                        Intent loginScreen = new Intent(RegisterActivity.this,LoginActivity.class);
                        startActivity(loginScreen);
                    }
                }

                else {
                    Toast.makeText(RegisterActivity.this, response, Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(RegisterActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {



                HashMap map  = new HashMap();
                map.put("first_name",firstname.getText().toString());
                map.put("last_name",lastname.getText().toString());
                map.put("email",email.getText().toString());
                map.put("password",password.getText().toString());
                map.put("mobile_number",mobilenumber.getText().toString());
                map.put("gender",Integer.toString(genderValue));
                map.put("type",Integer.toString(typeValue));
                if (birthdate.getText().toString()!="")
                    map.put("birthdate",birthdate.getText().toString());
                //upload photo to server
                //if(test.getText().toString()!="")
                    //code to upload photo

                return map;
            }
        };

        Volley.newRequestQueue(this).add(postrequest);

    }

    public  boolean getTheResponce(){

            userCreatedFlag = true;
        return  true;
    }

    //to set date on birthdate field
    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        birthdate.setText(sdf.format(myCalendar.getTime()));
    }

    //get image from galary
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            //ImageView imageView = (ImageView) findViewById(R.id.imgView);
            //imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            test.setText(picturePath);
        }


    }

    public boolean validation(){
        String testFirstname = firstname.getText().toString();
        String testLastname = lastname.getText().toString();
        String testEmail = email.getText().toString();
        String tesPassword = password.getText().toString();
        String tesconfirmpassword = confirmpassword.getText().toString();
        String testBirthdate = birthdate.getText().toString();
        String testMobilenumber = mobilenumber.getText().toString();

        if(TextUtils.isEmpty(testFirstname) || testFirstname.length() < 3) {
            firstname.setError("Please Enter fristname");
            return false;
        }
        if(TextUtils.isEmpty(testLastname) || testLastname.length() < 3) {
            lastname.setError("Please Enter lastname");
            return false;
        }
        if(TextUtils.isEmpty(testEmail) || !testEmail.contains("@") || !testEmail.contains(".")) {
            email.setError("Please Enter a valid email");
            return false;
        }
        if(TextUtils.isEmpty(tesPassword) || tesPassword.length() < 3) {
            password.setError("Please Enter a valid password");
            return false;
        }
        if(TextUtils.isEmpty(tesconfirmpassword) || !tesconfirmpassword.matches(tesPassword)) {
            confirmpassword.setError("Confirmed password must match the password");
            return false;
        }
        //if(TextUtils.isEmpty(testBirthdate) || !testBirthdate.contains("/")) {
        //    birthdate.setError("Please Enter password");
        //    return false;
        //}
        if(TextUtils.isEmpty(testMobilenumber) || !(testMobilenumber.length()==11)) {
            mobilenumber.setError("Please Enter a valid mobile number");
            return false;
        }

        //to validate birthdate
        if(!TextUtils.isEmpty(testBirthdate)) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date myDate;
            try {
                myDate = df.parse(birthdate.getText().toString());
                Calendar cal = Calendar.getInstance();
                Calendar cal2 = Calendar.getInstance();
                cal.setTime(myDate);
                int year = cal.get(Calendar.YEAR);
                if (year >= cal2.get(Calendar.YEAR)) {
                    birthdate.setError("Please Enter a valid birthdate");
                    return false;
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
