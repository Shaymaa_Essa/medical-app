package medicalapp.its2015.com.medicalapp;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class Map2Activity extends ActionBarActivity {

    int userIcon,hospitalIcon,pharmacyIcon,doctorIcon;

    GoogleMap theMap;
    LocationManager locMan;
    Marker userMarker = null;
    Marker [] placesMarker;
    MarkerOptions[] places;
    final  int MAX_PLACES = 20;
    public static final String SEARCH_TYPE = "type";
    String type;

    private class GetPlaces extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... stringURL) {
            StringBuilder jsonResponse = new StringBuilder();
            try {
                URL queryURL = new URL(stringURL[0]);
                HttpURLConnection myConnection = (HttpURLConnection) queryURL.openConnection();
                InputStreamReader streamReader = new InputStreamReader(myConnection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(streamReader);
                String lineIn;
                while ((lineIn = bufferedReader.readLine()) != null)
                    jsonResponse.append(lineIn);




                return jsonResponse.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return e.toString();
            } catch (IOException e) {
                e.printStackTrace();
                return e.toString();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if(placesMarker != null){
                for (Marker m : placesMarker)
                    if (m!=null)
                        m.remove();
            }

            try{
                JSONObject resultObject = new JSONObject(result);
                JSONArray placesArray = resultObject.getJSONArray("results");
                //Toast.makeText(Map2Activity.this,result.toString(), Toast.LENGTH_LONG).show();
                //Toast.makeText(Map2Activity.this,"test2", Toast.LENGTH_LONG).show();

                places = new MarkerOptions[placesArray.length()];
                for (int p=0 ;p<placesArray.length();p++){
                    LatLng placeLL = null;
                    String placeName = "";
                    String vicinity = "";
                    int currIcon = 0;

                    boolean missingValue = false;

                    try{
                        missingValue = false;
                        JSONObject placeObject = placesArray.getJSONObject(p);
                        JSONObject loc = placeObject.getJSONObject("geometry");
                        placeLL = new LatLng(
                                Double.valueOf(loc.getJSONObject("location").getString("lat")),
                                Double.valueOf(loc.getJSONObject("location").getString("lng"))
                        );

                        JSONArray types = placeObject.getJSONArray("types");
                        for (int t=0 ;t<types.length();t++){
                            String thisType = types.get(t).toString();

                            //Toast.makeText(Map2Activity.this, thisType, Toast.LENGTH_LONG).show();
                            if(thisType.contains("hospital")){
                                currIcon = hospitalIcon;
                                break;
                            }
                            else if(thisType.contains("pharmacy")){
                                currIcon = pharmacyIcon;
                                break;
                            }
                            else if(thisType.contains("doctor")){
                                currIcon = doctorIcon;
                                break;
                            }
                        }

                        vicinity = placeObject.getString("vicinity");
                        placeName = placeObject.getString("name");
                    }catch (JSONException e){
                        missingValue = true;
                        e.printStackTrace();
                    }
//
//                    userMarker = theMap.addMarker(new MarkerOptions()
//                            .position(lastLatLng)
//                            .title("Your Place")
//                            .icon(BitmapDescriptorFactory.fromResource(userIcon))
//                            .snippet("your last recorded location"));

                    //if(missingValue)
                        //places[p] = null;
                   // else
                        places[p] = new MarkerOptions()
                                .position(placeLL)
                                .title(placeName)
                                .icon(BitmapDescriptorFactory.fromResource(currIcon))
                                .snippet(vicinity);

                    //userMarker = theMap.addMarker(places[p]);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }catch (Exception e){
                Toast.makeText(Map2Activity.this,e.toString(), Toast.LENGTH_LONG).show();
            }

            if (places!=null && placesMarker!=null){
                for (int p=0;p<places.length && p<placesMarker.length;p++){
                    if (places[p] != null)
                        placesMarker[p] = theMap.addMarker(places[p]);
                }
            }


        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map2);


        //set Icons we use in the map
        userIcon = R.drawable.yellow_point;
        hospitalIcon = R.drawable.red_point;
        pharmacyIcon = R.drawable.blue_point;
        //labIcon = R.drawable.green_point;
        doctorIcon = R.drawable.purple_point;

        placesMarker = new Marker[MAX_PLACES];

        Intent i = getIntent();
        type = i.getStringExtra(SEARCH_TYPE);


        if (theMap == null){
            theMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.frag_map2activity_map)).getMap();

        }

        if(theMap != null){
            theMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
           updatePlaces();
        }
    }

    private void updatePlaces(){
        locMan = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
        String bestProvider = locMan.getBestProvider(criteria, true);
        Location lastLoc = locMan.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

//        // getting GPS status
//        boolean isGPSEnabled = locMan.isProviderEnabled(LocationManager.GPS_PROVIDER);
//        // getting network status
//        boolean isNWEnabled = locMan.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
//
//
//        if (!isGPSEnabled && !isNWEnabled)
//        {
//            // no network provider is enabled
//            Toast.makeText(Map2Activity.this,"no Gps or network provider available",Toast.LENGTH_LONG).show();
//        }
//        else
//        {
//            // First get location from Network Provider
//            if (isNWEnabled)
//                if (locMan != null)
//                    lastLoc = locMan.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//
//            // if GPS Enabled get lat/long using GPS Services
//            if (isGPSEnabled)
//                //if (locMan == null)
//                    if (locMan != null)
//                        lastLoc = locMan.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//        }

        if(lastLoc != null){
            double lat = lastLoc.getLatitude();
            double lng = lastLoc.getLongitude();

            Toast.makeText(Map2Activity.this,"lat = "+lat +" lng = "+lng,Toast.LENGTH_LONG).show();

             LatLng lastLatLng = new LatLng(lat,lng);

        if (userMarker != null)
            userMarker.remove();

        userMarker = theMap.addMarker(new MarkerOptions()
        .position(lastLatLng)
        .title("Your Place")
        .icon(BitmapDescriptorFactory.fromResource(userIcon))
        .snippet("your last recorded location"));

            theMap.animateCamera(CameraUpdateFactory.newLatLng(lastLatLng),3000,null);

        String placeSearchStr = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?" +
                "location="+lat+","+lng+
                "&radius=10000" +
                "&type=hospital"+
                "&key="+R.string.google_api_key;
            String url2 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+
                    lat+','+lng+
                    "&radius=1000&type="+type+"&key=AIzaSyCFj4MpiHKAU-QA6ASByni9xjU_TllrddA";

          new GetPlaces().execute(url2);
        }

        else
            Toast.makeText(Map2Activity.this, "null pointer", Toast.LENGTH_LONG).show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
