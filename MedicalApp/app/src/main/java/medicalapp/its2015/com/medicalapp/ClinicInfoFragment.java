package medicalapp.its2015.com.medicalapp;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ClinicInfoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ClinicInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ClinicInfoFragment extends Fragment {
    RecyclerView recyclerView;
    private String city;
    private String area;
    private String streat;
    private String building;
    private String flat_number;
    private String work_from;
    private String work_to;
    private String cost;
    public static final String JSON_ARRAY = "doctor_info";
    public static final String doctor_id="doctor_id";
    List<ClinicListData> DataList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.fragment_clinic_info, container, false);
        recyclerView=(RecyclerView)V.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        final String url = "http://medicalapp.site88.net/phpfiles/doctor_personalinfo.php";
        final StringRequest doctordatarequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Toast.makeText(getActivity().getApplicationContext(), response, Toast.LENGTH_LONG).show();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray(JSON_ARRAY);
                    DataList=new ArrayList<>();
                    for (int i = 1; i < jsonArray.length(); i++) {
                        JSONObject CurrentObject = jsonArray.getJSONObject(i);
                        city = CurrentObject.getString("city");
                        area = CurrentObject.getString("area");
                        streat = CurrentObject.getString("streat");
                        building = CurrentObject.getString("building");
                        flat_number = CurrentObject.getString("flat_number");
                        work_from = CurrentObject.getString("work_from");
                        work_to = CurrentObject.getString("work_to");
                        cost = CurrentObject.getString("cost");
                        ClinicListData clinicListData=new ClinicListData(city,area,streat,building,flat_number,work_from
                                ,work_to,cost);
                        DataList.add(clinicListData);



                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ClinicDataAdapter adapter=new ClinicDataAdapter(DataList);
                recyclerView.setAdapter(adapter);


            }








        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.toString();


            }}){

            // @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap params = new HashMap();
                params.put(doctor_id, "19");


                return params;
            }


        };



        Volley.newRequestQueue(getActivity().getApplicationContext()).add(doctordatarequest);



        return V;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);




    }


}
