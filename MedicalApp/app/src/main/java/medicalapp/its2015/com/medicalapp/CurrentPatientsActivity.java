package medicalapp.its2015.com.medicalapp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class CurrentPatientsActivity extends ActionBarActivity {


    ListView patientsList;
    PatientAdapter patientAdapter;
    EditText searchField;
    Button newPatient;

    List<Patient> originalPatientsList ;
    List<Patient> currentPatientsList = new ArrayList<Patient>(); //for search filter
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_patients);

        initialize(); // to initialize w.r.t layout fields

        webservice(); //volley library webservice

        patientAdapter = new PatientAdapter(this,R.layout.patientitem,currentPatientsList);
        patientsList.setAdapter(patientAdapter);

        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String filterString = charSequence.toString().toLowerCase();
                currentPatientsList.clear();
                if (TextUtils.isEmpty(filterString))
                    currentPatientsList.addAll(originalPatientsList);

                else {
                    String filterableString;
                    for (Patient patient : originalPatientsList) {
                        if (patient.getPatientName().contains(filterString)) {
                            currentPatientsList.add(patient);
                        }
                    }
                }


                patientAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        newPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CurrentPatientsActivity.this,NewPatientActivity.class);
                startActivity(i);
            }
        });
    }

    private void initialize() {
        patientsList = (ListView)findViewById(R.id.lview_currentpatients_patients);
        searchField = (EditText)findViewById(R.id.etxt_currentpatients_search);
        newPatient = (Button) findViewById(R.id.btn_currentpatients_addnewpatient);


    }

    private void webservice(){

        url = "http://medicalapp.site88.net/phpfiles/doctor_patients_getallpatients1.php";
        StringRequest postrequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.contains("all_patients")){
                     try{
                         JSONObject patientObject = new JSONObject(response);
                         JSONArray  patientArray = patientObject.getJSONArray("all_patients");
                         for (int i =0 ;i<patientArray.length();i++){
                             JSONObject currentObject = patientArray.getJSONObject(i);
                             String patientId = currentObject.getString("user_id");
                             String patientName = currentObject.getString("patient_name");
                             currentPatientsList.add(new Patient(patientId,patientName));
                         }

                         originalPatientsList = new ArrayList<Patient>();
                         for (Patient patient : currentPatientsList){
                             originalPatientsList.add(patient);
                         }
                         patientAdapter.setItemList(currentPatientsList);
                         patientAdapter.notifyDataSetChanged();


                     }catch (JSONException e){
                         e.printStackTrace();
                     }

                    //when item clicked should view the medical history of this patient
                    patientsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            Intent newMedicalHistory = new Intent(CurrentPatientsActivity.this,PatientMedicalHistoryActivity.class);
                            newMedicalHistory.putExtra(PatientMedicalHistoryActivity.USER_NAME,currentPatientsList.get(i).getPatientName());
                            newMedicalHistory.putExtra(PatientMedicalHistoryActivity.USER_ID,currentPatientsList.get(i).getPatientId());
                            startActivity(newMedicalHistory);
                        }
                    });

                }

                else {
                    Toast.makeText(CurrentPatientsActivity.this, response, Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(CurrentPatientsActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        Volley.newRequestQueue(this).add(postrequest);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_current_patients, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        patientAdapter.notifyDataSetChanged();
    }
}
