package medicalapp.its2015.com.medicalapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalInfoFragment extends Fragment {

    public static final String JSON_ARRAY = "doctor_info";

    public static final String KEY_NAME = "first_name";
    public static final String KEY_EMAIL = "email";
    public static final String Key_mobile="mobile_number";
    public static final String doctor_id="doctor_id";


    TextView textViewName;
    TextView textViewMobile;
    TextView textViewBirthday;
    TextView textViewEmail;
    TextView textViewGender;
    TextView textViewSpecialization;
    TextView textViewQualifications;


    public PersonalInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View V = inflater.inflate(R.layout.fragment_personal_info, container, false);
        textViewBirthday = (TextView) V.findViewById(R.id.txt_birthday);
        textViewName = (TextView) V.findViewById(R.id.textView_name);
        textViewMobile = (TextView) V.findViewById(R.id.textView_mobilenum);
        textViewEmail = (TextView) V.findViewById(R.id.textView_email);
        textViewGender=(TextView)V.findViewById(R.id.textView_gender);
        textViewSpecialization=(TextView)V.findViewById(R.id.textView_specialization);
        textViewQualifications=(TextView)V.findViewById(R.id.textView_Qualifications);


        final String url = "http://medicalapp.site88.net/phpfiles/doctor_personalinfo.php";
        final StringRequest doctordatarequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray(JSON_ARRAY);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject CurrentObject = jsonArray.getJSONObject(i);

                        String firstname = CurrentObject.getString(KEY_NAME);
                        String lastname=CurrentObject.getString("last_name");
                        String mobile = CurrentObject.getString(Key_mobile);
///                        String birthday=CurrentObject.getString("birthday");
                        String email = CurrentObject.getString(KEY_EMAIL);
                        String gender=CurrentObject.getString("gender");
                        String specialization=CurrentObject.getString("specialization");
                        String Qualifications=CurrentObject.getString("Qualifications");
                        textViewName.setText(firstname +" " + lastname );
                        textViewMobile.setText(mobile);
                        ///   textViewBirthday.setText(birthday);
                        textViewEmail.setText(email);
                        textViewGender.setText(gender);
                        textViewSpecialization.setText(specialization);
                        textViewQualifications.setText(Qualifications);


                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.toString();


            }}){

            // @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap params = new HashMap();
                params.put(doctor_id, "19");


                return params;
            }


        };



        Volley.newRequestQueue(getActivity().getApplicationContext()).add(doctordatarequest);



        return V;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);




    }


}
