package medicalapp.its2015.com.medicalapp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PatientMedicalHistoryActivity extends ActionBarActivity {
    TextView patientName;
    RecyclerView recyclerView ;

    String url;
    final static String USER_ID = "userId";
    final static String USER_NAME = "userName";
    String userId;
    String userName;


    List<MedicalHistoryItem> medicalHistoryItemList = new ArrayList<MedicalHistoryItem>();
    MedicalHistoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_medical_history);

        Intent i = getIntent();
        userId = i.getStringExtra(USER_ID);
        userName = i.getStringExtra(USER_NAME);

        patientName = (TextView)findViewById(R.id.txt_patientmedicalhistory_patientname);
        recyclerView = (RecyclerView) findViewById(R.id.recview_patientmedicalhistory_history);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        patientName.setText(userName);

        webservice();




    }

    private void webservice() {
        url = "http://medicalapp.site88.net/phpfiles/doctor_patients_patientmedicalhistory3.php";
        StringRequest postrequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.contains("patient_medicalhistory")){
                    try{
                        JSONObject historyObject = new JSONObject(response);
                        JSONArray historyArray = historyObject.getJSONArray("patient_medicalhistory");
                        for (int i =0 ;i<historyArray.length();i++){
                            JSONObject currentObject = historyArray.getJSONObject(i);
                            String doctorName = currentObject.getString("doctor_name");
                            String diagnosis = currentObject.getString("diagnosis");
                            String date = currentObject.getString("current_date");
                            StringBuilder medicine = new StringBuilder() ;
                            JSONArray medicineArray= currentObject.getJSONArray("medicine");
                            for(int j = 0 ;j<medicineArray.length();j++){
                                JSONObject medicineCurrentObject = medicineArray.getJSONObject(j);
                                medicine.append(medicineCurrentObject.getString("medicine_name")+ ", ");

                            }
                            medicalHistoryItemList.add(new MedicalHistoryItem(date,doctorName,diagnosis,medicine.toString()));
                        }

                    }catch (JSONException e){
                        e.printStackTrace();
                    }

                    adapter = new MedicalHistoryAdapter(medicalHistoryItemList);
                    recyclerView.setAdapter(adapter);

                    adapter.notifyDataSetChanged();
                }

                else {
                    Toast.makeText(PatientMedicalHistoryActivity.this, response, Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(PatientMedicalHistoryActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {



                HashMap map  = new HashMap();
                map.put("user_id",userId);

                return map;
            }
        };

        Volley.newRequestQueue(this).add(postrequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_patient_medical_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
