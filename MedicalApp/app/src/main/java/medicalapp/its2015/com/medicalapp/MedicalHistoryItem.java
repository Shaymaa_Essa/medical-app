package medicalapp.its2015.com.medicalapp;

/**
 * Created by MOSTAFA on 05/04/2016.
 */

//to store data of Medical history for specific patient
    //doctor app
public class MedicalHistoryItem {
    String date;
    String doctorName;
    String diagnosis;
    String medicine;

    public MedicalHistoryItem(String date,String doctorName , String diagnosis, String medicine){
        this.date = date;
        this.doctorName = doctorName;
        this.diagnosis = diagnosis;
        this.medicine = medicine;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getMedicine() {
        return medicine;
    }

    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }


}
