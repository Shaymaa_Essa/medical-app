package medicalapp.its2015.com.medicalapp;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;


public class Patient_HomeActivity extends ActionBarActivity {

    private String[] drawerTitles;
    private  int[] drawerIcons;
    private ListView drawerList;
    private DrawerLayout drawerLayout;
    TextView name;
    ListView todayDoctors;
    RecyclerView navigationMenu;
    NavigationDrawerAdapter navigationDrawerAdapter;
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout

    ActionBarDrawerToggle mDrawerToggle;

    String nameValue;
    String patientIdValue;
    String emailValue;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient__home);

        drawerTitles = getResources().getStringArray(R.array.patients_drawer_titles);
        drawerIcons = new int[]{R.drawable.personal_info,R.drawable.current_patients,R.drawable.patient_medicalstate,R.drawable.patient_appointments,R.drawable.searchforplace};


        nameValue = ((MyAppConstants) this.getApplication()).getUserFirstName()+" "+
                ((MyAppConstants) this.getApplication()).getUserLastName();
        emailValue = ((MyAppConstants) this.getApplication()).getUserEmail();
        patientIdValue = ((MyAppConstants) this.getApplication()).getUserId();

        navigationMenu = (RecyclerView) findViewById(R.id.recview_patienthome_navigationdrawer);
        navigationMenu.setHasFixedSize(true);
        navigationDrawerAdapter = new NavigationDrawerAdapter(drawerTitles,drawerIcons,nameValue,emailValue,R.drawable.patientimg,getApplicationContext(),1);


        name = (TextView)findViewById(R.id.txt_patienthome_welcome);
        name.setText("Welcome "+nameValue);


        navigationMenu.setAdapter(navigationDrawerAdapter);                              // Setting the adapter to RecyclerView

        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager

        navigationMenu.setLayoutManager(mLayoutManager);                 // Setting the layout Manager

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_patient__home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
