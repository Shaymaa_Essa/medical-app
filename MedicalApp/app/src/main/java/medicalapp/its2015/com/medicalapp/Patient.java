package medicalapp.its2015.com.medicalapp;

/**
 * Created by MOSTAFA on 23/03/2016.
 */
public class Patient {
    String patientId;
    String patientName;

    public Patient (String patientId , String patientName){
        this.patientId = patientId;
        this.patientName = patientName;
    }

    public String getPatientId() {
        return patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    @Override
    public String toString() {
        return patientName;
    }
}
