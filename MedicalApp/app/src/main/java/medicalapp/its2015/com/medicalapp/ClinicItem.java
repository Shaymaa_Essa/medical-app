package medicalapp.its2015.com.medicalapp;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by MOSTAFA on 03/04/2016.
 */
public class ClinicItem  implements Parcelable {
    String lat;
    String lng;
    String city;
    String area;
    String street;
    String building;
    String flatNumber;
    String workFrom;
    String workTo;

    public ClinicItem(String lat,String lng,String city,String area,String street,String building,String flatNumber,String workFrom,String workTo){
        this.lat = lat;
        this.lng = lng;
        this.city = city;
        this.area = area;
        this.street = street;
        this.building = building;
        this.flatNumber = flatNumber;
        this.workFrom = workFrom;
        this.workTo = workTo;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(String flatNumber) {
        this.flatNumber = flatNumber;
    }

    public String getWorkFrom() {
        return workFrom;
    }

    public void setWorkFrom(String workFrom) {
        this.workFrom = workFrom;
    }

    public String getWorkTo() {
        return workTo;
    }

    public void setWorkTo(String workTo) {
        this.workTo = workTo;
    }

    @Override
    public String toString() {
        return flatNumber+","+building+","+street+","+city;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
