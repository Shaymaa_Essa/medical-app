package medicalapp.its2015.com.medicalapp;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class PatientInfoActivity extends ActionBarActivity {

    public static final String JSON_ARRAY = "patient_info";
    String user_id;
    TextView textViewName;
    TextView textViewMobile;
    TextView textViewBirthday;
    TextView textViewEmail;
    TextView textViewGender;
    TextView textViewPersonalInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_info);
        textViewPersonalInfo = (TextView) findViewById(R.id.textView_patient_personalinfo);
        textViewBirthday = (TextView) findViewById(R.id.textView_patient_birthday);
        textViewName = (TextView) findViewById(R.id.textView_patient_name);
        textViewMobile = (TextView) findViewById(R.id.textView_patient_mobile);
        textViewEmail = (TextView) findViewById(R.id.textView_patient_email);
        textViewGender = (TextView) findViewById(R.id.textView_patient_gender);
        user_id = ((MyAppConstants)this.getApplication()).getUserId();

        final String url = "http://medicalapp.site88.net/phpfiles/patient_personalinfo.php";
        final StringRequest patientdatarequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                  
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray(JSON_ARRAY);


                        JSONObject CurrentObject = jsonArray.getJSONObject(0);

                        String firstname = CurrentObject.getString("first_name");
                        String lastname = CurrentObject.getString("last_name");
                        String mobile = CurrentObject.getString("mobile_number");
///                        String birthday=CurrentObject.getString("birthday");
                        String email = CurrentObject.getString("email");
                        String gender = CurrentObject.getString("gender");

                        textViewName.setText(firstname + " " + lastname);
                        textViewMobile.setText(mobile);
                        //  textViewBirthday.setText(birthday);
                        textViewEmail.setText(email);

                    if(gender.contains("0"))
                        textViewGender.setText("Male");
                    else if(gender.contains("1"))
                        textViewGender.setText("Female");


                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.toString();


            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap params = new HashMap();
                params.put("user_id",user_id);


                return params;
            }


        };


        Volley.newRequestQueue(this).add(patientdatarequest);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_patient_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
