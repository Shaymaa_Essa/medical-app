package medicalapp.its2015.com.medicalapp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


/**
 * Created by MOSTAFA on 05/04/2016.
 */
public class MedicalHistoryAdapter extends RecyclerView.Adapter<MedicalHistoryAdapter.MedicalHistoryHolder> {

    class MedicalHistoryHolder extends RecyclerView.ViewHolder{
        TextView date;
        TextView doctorName;
        TextView diagnosis;
        TextView medicine;




        public MedicalHistoryHolder(View itemView) {
            super(itemView);
            date = (TextView)itemView.findViewById(R.id.txt_medicalhistoryitem_date);
            doctorName = (TextView)itemView.findViewById(R.id.txt_medicalhistoryitem_doctorname);
            diagnosis = (TextView)itemView.findViewById(R.id.txt_medicalhistoryitem_diagnosis);
            medicine = (TextView)itemView.findViewById(R.id.txt_medicalhistoryitem_medicine);
        }
    }

    List<MedicalHistoryItem> medicalHistoryItemList ; //list which contains data of medical history
    @Override
    public MedicalHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.medicalhistoryitem,parent,false);
        MedicalHistoryHolder holder = new MedicalHistoryHolder(row);
        return holder;
    }

    public MedicalHistoryAdapter(List<MedicalHistoryItem> medicalHistoryItemList){
        this.medicalHistoryItemList = medicalHistoryItemList;
    }

    @Override
    public void onBindViewHolder(MedicalHistoryHolder holder, int position) {
        MedicalHistoryItem historyItem = medicalHistoryItemList.get(position);
        holder.date.setText("Date: "+historyItem.getDate().toString());
        holder.doctorName.setText("DR name: "+historyItem.getDoctorName().toString());
        holder.diagnosis.setText("Diagnosis: "+historyItem.getDiagnosis().toString());
        holder.medicine.setText("Medicine: "+historyItem.getMedicine().toString());
    }


    @Override
    public int getItemCount() {
        return medicalHistoryItemList.size();
    }
}
